import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {
  
  BASE_URL: string = "http://localhost:2121/"
  httpOptions: any;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  getStudents(src: string): Observable<any> {
    return this.http.get<any>(this.BASE_URL + `students?${src ? `src=${src}` : ''}`);
  }

  getOneStudent(arg0: string) {
    return this.http.get<any>(this.BASE_URL + `students/${arg0}`);
  }

  saveStudent(studentData: any): Observable<any> {
    return this.http.post(this.BASE_URL + "students", studentData, this.httpOptions);
  }

  updateStudent(id: string, studentData: any): Observable<any> {
    return this.http.put(this.BASE_URL + `students/${id}`, studentData, this.httpOptions);
  }

  deleteStudent(id: string): Observable<any> {
    return this.http.delete(this.BASE_URL + `students/${id}`);
  }

  borrowBook(reqBody: any) {
    return this.http.post(this.BASE_URL + "students/borrow", reqBody, {responseType: 'text'});
  }

  returnBook(reqBody: any) {
    return this.http.post(this.BASE_URL + "students/return", reqBody, {responseType: 'text'});
  }

  getBooks(src: string): Observable<any> {
    return this.http.get<any>(this.BASE_URL + `books?${src ? `src=${src}` : ''}`);
  }

  getOneBook(arg0: any) {
    return this.http.get<any>(this.BASE_URL + `books/${arg0}`);
  }
  
  saveBook(bookData: any) {
    return this.http.post(this.BASE_URL + "books", bookData, this.httpOptions);
  }

  updateBook(id: string, bookData: any): Observable<any> {
    return this.http.put(this.BASE_URL + `books/${id}`, bookData, this.httpOptions);
  }

  getTransHist(arg0: string) {
    return this.http.get<any>(this.BASE_URL + `trans/${arg0}`);
  }
}
