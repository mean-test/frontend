import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { ToastrModule } from 'ngx-toastr'

import { AppComponent } from './app.component';
import { ApiService } from './api.service';
import { StudentsComponent } from './students/students.component';
import { StudentDetailsComponent } from './students/student-details/student-details.component';
import { BooksComponent } from './books/books.component';
import { BookDetailsComponent } from './books/book-details/book-details.component';
import { BorrowComponent } from './borrow/borrow.component';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentDetailsComponent,
    BooksComponent,
    BookDetailsComponent,
    BorrowComponent,
    FieldErrorDisplayComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'students', component: StudentsComponent },
      { path: 'students/:docId', component: StudentDetailsComponent },
      { path: 'books', component: BooksComponent },
      { path: 'books/:docId', component: BookDetailsComponent },
      { path: 'borrow', component: BorrowComponent },]),
      BrowserAnimationsModule, // required animations module
      ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
