import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  book: any;
  bookForm: FormGroup;

  constructor(private api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private form: FormBuilder,
    private toast: ToastrService) {
    this.bookForm = this.form.group({
      title: new FormControl('', [Validators.required]),
      author: new FormControl('', [Validators.required]),
      genre: new FormControl('', [Validators.required]),
      pageCount: new FormControl('', [Validators.required, Validators.min(0)]),
      publishYear: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.min(0)]),
      stock: new FormControl('', [Validators.required, Validators.min(0)]),
    })
  }

  ngOnInit() {
    this.api.getOneBook(this.route.snapshot.params['docId']).subscribe((data) => {
      this.book = data;
      this.bookForm.setValue({
        title: data.title,
        author: data.author, genre: data.genre, pageCount: data.pageCount, publishYear: data.publishYear,
        stock: data.stock
      })
    });
  }

  updateBook(bookData: any) {
    console.log(bookData);
    if (this.bookForm.valid) {
      bookData.publishYear = parseInt(bookData.publishYear);
      this.api.updateBook(this.route.snapshot.params['docId'], bookData).subscribe((data) => {
        this.toast.success("Data successfully saved.");
        this.book = data;
        this.bookForm.setValue({
          title: data.title,
          author: data.author, genre: data.genre, pageCount: data.pageCount, publishYear: data.publishYear,
          stock: data.stock
        })
      });
    }
  }
}
