import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: any;
  srcForm: FormGroup;
  bookForm: FormGroup;

  constructor(private api: ApiService,
    private form: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService) {
    this.srcForm = this.form.group({
      query: new FormControl('')
    });
    this.bookForm = this.form.group({
      title: new FormControl('', [Validators.required]),
      author: new FormControl('', [Validators.required]),
      genre: new FormControl('', [Validators.required]),
      pageCount: new FormControl('', [Validators.required, Validators.min(0)]),
      publishYear: new FormControl('', [Validators.required, Validators.minLength(4),
      Validators.maxLength(4), Validators.min(0)]),
    })
  }

  ngOnInit() {
    let src = this.route.snapshot.queryParams.src;
    if (src) this.srcForm.setValue({ query: src });

    this.api.getBooks(src ? src : '').subscribe(data => {
      this.books = data;
    });
  }

  search(query: string) {
    this.router.navigate([`books`], { queryParams: { src: query } });
    this.api.getBooks(query).subscribe(data => {
      this.books = data;
    });
  }

  isFieldValid(field: string) {
    return !this.bookForm.get(field).valid && this.bookForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  addBook(bookData: any) {
    console.log(bookData);
    if (this.bookForm.valid) {
      bookData.publishYear = parseInt(bookData.publishYear);
      this.api.saveBook(bookData).subscribe((data) => {
        this.toast.success("Data successfully saved.");
        this.ngOnInit();
      });
    }
    else {
      this.validateAllFormFields(this.bookForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
