import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.css']
})
export class BorrowComponent implements OnInit {
  borrowForm: FormGroup;
  students: any;
  books: any;

  constructor(private api: ApiService,
    private form: FormBuilder,
    private toast: ToastrService) {
    this.borrowForm = this.form.group({
      student: [null, [Validators.required]],
      book: [null, [Validators.required]]
    });
  }

  ngOnInit() {
    this.api.getBooks('').subscribe(data => {
      this.books = data;
    });
    this.api.getStudents('').subscribe(data => {
      this.students = data;
    });
  }

  process(formData) {
    console.log(formData);
    this.api.borrowBook({userId:formData.student, bookId:formData.book}).subscribe(data => {
      this.toast.success(data);
      this.borrowForm.reset();
    })
  }
}
