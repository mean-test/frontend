import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {
  student: any
  stdForm: FormGroup
  transHist: any

  constructor(private api: ApiService,
    private form: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private _location: Location) {
    this.stdForm = this.form.group({
      name: new FormControl('', [Validators.required]),
      birthDate: new FormControl('', [Validators.required])
    })
  }

  ngOnInit() {
    this.api.getOneStudent(this.route.snapshot.params['docId']).subscribe((data) => {
      this.student = data;
      this.stdForm.setValue({ name: data.name, birthDate: this.convertDateToString(new Date(data.birthDate)) });
    });
    this.api.getTransHist(this.route.snapshot.params['docId']).subscribe((data) => {
      this.transHist = data;
    });
  }

  convertDateToString(date: Date){
    return `${date.getUTCFullYear()}-${date.getUTCMonth()+1}-${date.getUTCDate()}`
  }

  return(bookId: string) {
    this.api.returnBook({ userId: this.student._id, bookId: bookId }).subscribe((data) => {
      this.toast.success(data);
      this.api.getOneStudent(this.route.snapshot.params['docId']).subscribe((data) => {
        this.student = data;
      });
      this.api.getTransHist(this.route.snapshot.params['docId']).subscribe((data) => {
        this.transHist = data;
      });
    })
  }

  updateStudent(studentData) {
    console.log(studentData);
    if(this.stdForm.valid){
      this.api.updateStudent(this.route.snapshot.params['docId'],studentData).subscribe((data)=>{
        this.toast.success("Updated Successfully");
        this.api.getOneStudent(this.route.snapshot.params['docId']).subscribe((data) => {
          this.student = data;
        });
      })
    }
  }
}
