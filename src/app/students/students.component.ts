import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students: any;
  srcForm: FormGroup;
  stdForm: FormGroup;

  constructor(private api: ApiService,
    private form: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastrService) {
      this.srcForm = this.form.group({
        query: new FormControl('')
      })
      this.stdForm = this.form.group({
        name: new FormControl('',[Validators.required]),
        birthDate: new FormControl('',[Validators.required])
      })
  }

  ngOnInit() {
    let src = this.route.snapshot.queryParams.src;
    if (src) this.srcForm.setValue({ query: src });

    this.api.getStudents(src ? src : '').subscribe(data => {
      this.students = data;
    });
  }

  search(query: string) {
    this.router.navigate([`students`], { queryParams: { src: query } });
    this.api.getStudents(query).subscribe(data => {
      this.students = data;
    });
  }

  isFieldValid(field: string) {
    return !this.stdForm.get(field).valid && this.stdForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  addStudent(stdData: any){
    console.log(stdData);

    if(this.stdForm.valid){
      this.api.saveStudent(stdData).subscribe((data)=>{
        this.toast.success("Data successfully saved.");
        this.ngOnInit();
      })
    }
    else{
      this.validateAllFormFields(this.stdForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
